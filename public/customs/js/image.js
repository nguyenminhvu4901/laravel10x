/* global FileReader */
'use strict'
import { IMAGE_DEFAULT_PRODUCT } from '../products/js/constant.js'

export const image = (function () {
  const module = {}

  module.load = function (element) {
    const file = $(element)[0].files[0]
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => {
      const url = URL.createObjectURL(file)
      $('.image-preview').attr('src', url)
    }
  }

  module.remove = function () {
    $('.image-preview').attr('src', IMAGE_DEFAULT_PRODUCT)
  }

  return module
})(window.jQuery, document, window)
