<?php

namespace App\Repositories;

use App\Models\Product;
use App\Repositories\BaseRepository;

class ProductRepository extends BaseRepository
{
    public function model()
    {
        return Product::class;
    }

    public function search(array $data)
    {
        return $this->model->withName($data['name'])
            ->withCategoryId($data['category'])
            ->latest('id')
            ->paginate(10);
    }
}
