<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\BaseRepository;

class UserRepository extends BaseRepository
{
    public function model()
    {
        return User::class;
    }

    public function search(array $data)
    {
        return $this->model->withName($data['name'] ?? null)
            ->withRole($data['role'] ?? null)
            ->latest('id')
            ->paginate(10);
    }
}
