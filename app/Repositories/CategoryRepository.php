<?php

namespace App\Repositories;

use App\Models\Category;
use App\Repositories\BaseRepository;

class CategoryRepository extends BaseRepository
{
    public function model()
    {
        return Category::class;
    }

    public function search(array $data)
    {
        return $this->model->withNameLikeCategory($data['name'] ?? null)
            ->withParentId($data['parent_id'] ?? null)
            ->latest('id')
            ->paginate(10);
    }
}
