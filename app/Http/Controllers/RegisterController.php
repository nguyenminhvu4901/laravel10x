<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Rules\EmailCompanyRule;
use App\Rules\VietnamPhoneNumber;
use App\Http\Controllers\Controller;
use App\Http\Requests\Users\StoreRequest;

class RegisterController extends Controller
{
    public function create()
    {
        return view('register.create');
    }

    public function store(StoreRequest $request)
    {

        $data = $request->all();
        $attributes = request()->validate([
            'name' => 'required|max:255',
            'email' => ['required', 'email',  new EmailCompanyRule, 'unique:' . User::class],
            'phone' => ['required', new VietnamPhoneNumber],
            'password' => ['required', 'string', 'min:8'],
        ]);

        $user = User::create($data);
        auth()->login($user);

        return redirect()->route('users.information');
    }
}
