<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $user = Auth::user();
        $checkShow = $user->hasPermission('products.show');
        $checkEdit = $user->hasPermission('products.edit');
        $checkUpdate = $user->hasPermission('products.update');
        $checkDelete = $user->hasPermission('products.destroy');

        return  [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'description' => $this->description,
            'expiry' => $this->expiry,
            'image' => $this->fullPathImage,
            'categories' =>
            [
                'ids' => implode(',', $this->categories->pluck('id')->toArray()),
                'names' => implode(',', $this->categories->pluck('name')->toArray())
            ],
            'show' => ['isShow' => $checkShow, 'link' => route('products.show', $this->id)],
            'edit' => ['isShow' => $checkEdit, 'link' => route('products.edit', $this->id)],
            'update' => ['isShow' => $checkUpdate, 'link' => route('products.update', $this->id)],
            'delete' => ['isShow' => $checkDelete, 'link' => route('products.destroy', $this->id)],
        ];
    }
}
