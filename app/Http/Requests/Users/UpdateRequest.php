<?php

namespace App\Http\Requests\Users;

use App\Models\User;
use App\Rules\EmailCompanyRule;
use Illuminate\Validation\Rule;
use App\Rules\VietnamPhoneNumber;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'email' => [
                'email',
                'max:255',
                Rule::unique(User::class)->ignore($this->id),
                new EmailCompanyRule
            ],
            'phone' => ['required', new VietnamPhoneNumber],
        ];
    }
}
