<?php

namespace App\Services;

use App\Traits\HandleImage;
use Illuminate\Http\Request;
use App\Repositories\ProductRepository;
use App\Http\Requests\Products\StoreRequest;
use App\Http\Requests\Products\UpdateRequest;

class ProductService
{
    use HandleImage;

    public $folder = 'products';

    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function all()
    {
        return $this->productRepository->all();
    }

    public function search(Request $request = null)
    {
        $data = $request->all();
        return $this->productRepository->search($data);
    }

    public function paginate($page = 5)
    {
        return $this->productRepository->paginate($page);
    }

    public function create(StoreRequest $request)
    {
        $data = $request->all();
        $data['image'] = $this->saveImage($request->image);
        $product = $this->productRepository->create($data);
        $categories = $request->input('categories');

        if ($categories !== null && isset($categories[0])) {
            $product->syncCategories($categories);
        }

        return $product;
    }

    public function findOrFail(string $id)
    {
        return $this->productRepository->findOrFail($id);
    }

    public function update(UpdateRequest $request, string $id)
    {
        $product = $this->productRepository->findOrFail($id);
        $data = $request->all();

        $data['image'] = $this->updateImage($request->image, $product->image);
        $product = $this->productRepository->update($data, $id);
        $categories = $request->input('categories');

        if ($categories !== null && isset($categories[0])) {
            $product->syncCategories($categories);
        }
        return $product;
    }

    public function delete(string $id): bool
    {
        $product = $this->productRepository->findOrFail($id);
        $this->deleteImage($product->image);
        return $this->productRepository->delete($id);
    }
}
