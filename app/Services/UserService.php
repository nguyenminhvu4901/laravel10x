<?php

namespace App\Services;

use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;

class UserService
{
    protected $userRepository;
    protected $roleRepository;
    public function __construct(UserRepository $userRepository, RoleRepository $roleRepository)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    public function all()
    {
        return $this->userRepository->all();
    }

    public function paginate($page)
    {
        return $this->userRepository->paginate($page);
    }

    public function search($request)
    {
        $data = $request->all();
        return $this->userRepository->search($data);
    }

    public function listRoles()
    {
        return $this->roleRepository->all();
    }

    public function create($request)
    {
        $data = $request->all();
        $user = $this->userRepository->create($data);
        $user->syncRoles($request->input('roles'));

        return $user;
    }

    public function findOrFail($id)
    {
        return $this->userRepository->findOrFail($id);
    }

    public function update($request, $id)
    {
        $data = $request->all();
        $user = $this->userRepository->update($data, $id);
        $user->syncRoles($request->input('roles'));

        return $user;
    }

    public function delete($id)
    {
        $this->userRepository->delete($id);
    }
}
