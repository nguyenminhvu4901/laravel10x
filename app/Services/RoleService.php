<?php

namespace App\Services;

use App\Repositories\RoleRepository;
use App\Repositories\PermissionRepository;

class RoleService
{
    protected $roleRepository;
    protected $permissionRepository;

    public function __construct(RoleRepository $roleRepository, PermissionRepository $permissionRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function all()
    {
        return $this->roleRepository->all();
    }

    public function paginate($page)
    {
        return $this->roleRepository->paginate($page);
    }

    public function search($request)
    {
        $data = $request->all();
        return $this->roleRepository->search($data);
    }

    public function listPermissions()
    {
        return $this->permissionRepository->all();
    }

    public function findOrFail($id)
    {
        return $this->roleRepository->findOrFail($id);
    }
    public function store($request)
    {
        $data = $request->all();
        $role = $this->roleRepository->create($data);
        $role->syncPermissions($request->input('permissionIds'));

        return $role;
    }
    public function update($request, $id)
    {
        $data = $request->all();

        $role = $this->roleRepository->update($data, $id);
        $permissionIds = $request->input('permissionIds');
        $role->syncPermissions($permissionIds);

        return $role;
    }

    public function delete($id)
    {
        return $this->roleRepository->delete($id);
    }
}
