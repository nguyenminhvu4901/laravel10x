<?php

namespace App\Traits\UserTraits;

trait ChecksUserTrait
{
    const ROLE_SUPER_ADMIN = 'superadmin';

    public function hasPermission($permissionName): bool
    {
        if ($this->isSuperAdmin()) {
            return true;
        }

        foreach ($this->roles as $role) {
            if ($role->hasPermission($permissionName)) {
                return true;
            }
        }

        return false;
    }

    public function isSuperAdmin(): bool
    {
        return $this->hasRole(self::ROLE_SUPER_ADMIN);
    }
}
