<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';

    protected $fillable = ['name', 'parent_id'];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'category_product');
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function scopeWithNameLikeCategory(Builder $query, string $name = null)
    {
        return $name ?
            $query->when($name, fn ($innerQuery) => $innerQuery->where('name', 'like', '%' . $name . '%')) :
            $query;
    }

    public function scopeWithParentId(Builder $query, string $id = null)
    {
        return $id ? $query->where('parent_id', $id) : $query;
    }
}
