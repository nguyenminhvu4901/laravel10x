<?php

namespace App\Models;

use App\Models\Role;
use App\Traits\UserTraits\ChecksUserTrait;
use Illuminate\Database\Eloquent\Builder;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, ChecksUserTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'location',
        'phone',
        'about',
        'password_confirmation'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute(string $password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }

    public function syncRoles($roles)
    {
        $this->roles()->sync($roles);
    }

    public function hasRole($roleName): bool
    {
        return $this->roles()->where('name', $roleName)->exists();
    }

    public function scopeWithName(Builder $query, string $name = null)
    {
        return $query->when($name, fn ($innerQuery) => $innerQuery->where('name', 'like', '%' . $name . '%'));
    }

    public function scopeWithRole(Builder $query, string $role = null)
    {
        return $role ? $query->whereHas('roles', fn ($innerQuery) => $innerQuery->where('role_id', $role)) : $query;
    }
}
