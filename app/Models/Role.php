<?php

namespace App\Models;

use App\Models\User;
use App\Models\Permission;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Role extends Model
{
    use HasFactory;

    protected $table = 'roles';

    protected $fillable = ['display_name', 'name'];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'permission_role');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'role_user', 'role_id', 'user_id');
    }

    public function syncPermissions($permission)
    {
        return $this->permissions()->sync($permission);
    }

    public function hasPermission($permissionName): bool
    {
        return $this->permissions->pluck('name')->contains($permissionName);
    }

    public function scopeWithName(Builder $query, string $name = null)
    {
        return $query->when($name, fn ($innerQuery) => $innerQuery->where('name', 'like', '%' . $name . '%'));
    }
}
