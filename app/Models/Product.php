<?php

namespace App\Models;

use App\Models\Category;
use App\Traits\HandleImage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory, HandleImage;

    protected $table = 'products';

    protected $fillable = ['name', 'price', 'description', 'expiry', 'image'];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_product', 'product_id', 'category_id');
    }

    public function attachCategories(array $categories)
    {

        $this->categories()->attach($categories);
    }

    public function syncCategories(array $categories)
    {
        $this->categories()->sync($categories);
    }

    protected function folder(): Attribute
    {
        return Attribute::make(
            get: fn () => config("constant.folder_image.products"),
        );
    }

    public function fullPathImage(): Attribute
    {
        return Attribute::make(
            get: fn () => ($this->getFullPath($this->image)),
        );
    }

    public function scopeWithName(Builder $query, string $name = null)
    {
        return $query->when($name, fn ($innerQuery) => $innerQuery->where('name', 'like', '%' . $name . '%'));
    }

    public function scopeWithCategoryId(Builder $query, string $category = null)
    {
        return $category ?
            $query->whereHas('categories', fn ($innerQuery) => $innerQuery->where('category_id', $category)) :
            $query;
    }
}
