module.exports = {
  extends: [
    'semistandard',
    'standard'
  ],
  globals: {
    $: true
  }
}
