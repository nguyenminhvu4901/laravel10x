<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;

Route::prefix('/categories')->controller(CategoryController::class)->name('categories.')->group(function () {
    Route::delete('/{id}', 'destroy')->name('destroy')->middleware(['hasPermission:categories.destroy']);
    Route::get('/getListCategory', 'getListCategory')->name('getListCategory');
    Route::get('/', 'index')->name('index')->middleware(['hasPermission:categories.index']);
    Route::get('/create', 'create')->name('create')->middleware(['hasPermission:categories.create']);
    Route::post('/', 'store')->name('store')->middleware(['hasPermission:categories.store']);
    Route::get('/{id}', 'show')->name('show')->middleware(['hasPermission:categories.show']);
    Route::get('/{id}/edit', 'edit')->name('edit')->middleware(['hasPermission:categories.edit']);
    Route::put('/{id}', 'update')->name('update')->middleware(['hasPermission:categories.update']);
});
