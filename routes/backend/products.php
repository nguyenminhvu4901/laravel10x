<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

Route::prefix('/products')->controller(ProductController::class)->name('products.')->group(function () {
    Route::get('/', 'index')->name('index')->middleware('hasPermission:products.index');
    Route::post('/list', 'list')->name('list');
    Route::get('/create', 'create')->name('create')->middleware('hasPermission:products.create');
    Route::post('/', 'store')->name('store')->middleware('hasPermission:products.store');
    Route::get('/{id}', 'show')->name('show')->middleware('hasPermission:products.show');
    Route::get('/{id}/edit', 'edit')->name('edit')->middleware('hasPermission:products.edit');
    Route::put('/{id}/update', 'update')->name('update')->middleware('hasPermission:products.update');
    Route::delete('/{id}', 'destroy')->name('destroy')->middleware('hasPermission:products.destroy');
});
