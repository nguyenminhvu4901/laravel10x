<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;

Route::prefix('/roles')->controller(RoleController::class)->name('roles.')->group(function () {
    Route::get('/', 'index')->name('index')->middleware('hasPermission:roles.index');
    Route::get('/create', 'create')->name('create')->middleware('hasPermission:roles.create');
    Route::post('/store', 'store')->name('store')->middleware('hasPermission:roles.store');
    Route::get('/{id}', 'show')->name('show')->middleware('hasPermission:roles.show');
    Route::get('/{id}/edit', 'edit')->name('edit')->middleware('hasPermission:roles.edit');
    Route::put('/{id}', 'update')->name('update')->middleware('hasPermission:roles.update');
    Route::delete('/{id}', 'destroy')->name('destroy')->middleware('hasPermission:roles.destroy');
});
