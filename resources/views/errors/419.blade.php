<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <title>419</title>
    <link rel="stylesheet" href="{{ asset('assets/errors/css/style.css') }}">
</head>
<body>
    <img src="{{ asset('customs/images/419.png') }}" alt="" width="90%">
    <a href="{{ route('users.information') }}" class="btn" style="text-decoration: none; background-color: red"><i class="fa fa-home"></i>Dashboard</a>
</body>
</html>