@extends('dashboard.index')

@section('content')
<div class="container-fluid py-4">
            
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-body px-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0">
                            <thead>
                                <tr>
                                    <th
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        #</th>
                                    <th
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Name</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Email</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Role</th>
                                </tr>
                            </thead>
                            <tbody>                                   
                                <tr>
                                    <td>
                                        {{ $user->id}}
                                    </td>
                                    <td>
                                        {{ $user->name}}
                                    </td>
                                    <td>
                                        {{ $user->email}}
                                    </td>
                                    <td class="align-middle text-center text-sm">
                                        @if($user->roles)
                                            @foreach($user->roles as $role)
                                                <span class="badge badge-sm bg-gradient-success">                                                 
                                                    {!! $role->display_name . '<br>'!!}                                                
                                                </span>
                                            @endforeach
                                        @endif
                                    </td>      
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-footers.auth></x-footers.auth>
</div>
@endsection