<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $superAdmin = User::create([
            'id' => 1,
            'name' => 'Super Admin',
            'email' => 'superadmin@deha-soft.com',
            'password' => 12345678,
            'phone' => '0384267182',
        ]);
        
        $superAdmin->roles()->attach(1);
        auth()->login($superAdmin);
        
        $admin = new User();
        $admin = User::create([
            'id' => 2,
            'name' => 'Admin',
            'email' => 'admin@deha-soft.com',
            'password' => 12345678,
            'phone' => '0384267182',
        ]);
        $admin->roles()->attach(2);
        auth()->login($admin);

        $user = User::create([
            'id' => 3,
            'name' => 'User',
            'email' => 'user@deha-soft.com',
            'password' => 12345678,
            'phone' => '0384267182',
        ]);
        $user->roles()->attach(3);
        auth()->login($user);
    }
}
