<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $superAdminRole = Role::create([
            'id' => 1,
            'display_name' => 'Super Admin',
            'name' => 'superadmin',
        ]);
        $superAdminRole->permissions()->attach(1);

        $adminRole = Role::create([
            'id' => 2,
            'display_name' => 'Admin',
            'name' => 'admin',
        ]);
        $adminRole->permissions()->attach([2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14]);

        $userRole = Role::create([
            'id' => 3,
            'display_name' => 'User',
            'name' => 'user',
        ]);
        $userRole->permissions()->attach([2, 5, 9, 12]);
    }
}
