<?php

namespace Tests\Feature\Products;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetListProductTest extends TestCase
{
    public function getRouteIndex()
    {
        return route('products.index');
    }
    /**
     * @test
     */
    public function authenticated_user_can_get_list_product()
    {
        $this->loginUserSuperAdmin();
        $response = $this->get($this->getRouteIndex());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('clients.products.index')
            ->assertSee('id')
            ->assertSee('name')
            ->assertSee('price')
            ->assertSee('description')
            ->assertSee('expiry')
            ->assertSee('category');
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_get_list_product()
    {
        $response = $this->get($this->getRouteIndex());

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticate_user_can_not_get_list_product_if_user_has_not_permission()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $response = $this->get($this->getRouteIndex());

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function loginUserSuperAdmin()
    {
        $user = User::where('email', 'superadmin@deha-soft.com')->first();
        $this->actingAs($user);
    }
}
