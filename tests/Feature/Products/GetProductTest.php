<?php

namespace Tests\Feature\Products;

use Tests\TestCase;
use App\Models\User;
use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetProductTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_get_products()
    {
        $this->loginUserSuperAdmin();
        $product = Product::factory()->create();
        $response = $this->getJson($this->getRouteShow($product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('status_code')
                ->has(
                    'data',
                    fn (AssertableJson $json) => $json
                        ->where('name', $product->name)
                        ->where('expiry', $product->expiry)
                        ->where('description', $product->description)
                        ->etc()
                )
                ->etc()
        );
        $response->assertJsonStructure(
            [
                'status_code',
                'data' => ['id', 'name', 'price', 'description', 'expiry', 'image', 'categories' => ['ids', 'names']],
            ]
        );
    }

    /** @test */
    public function authenticated_user_can_not_get_products_if_product_not_exits()
    {
        $this->loginUserSuperAdmin();
        $response = $this->getJson($this->getRouteShow(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('status_code')
                ->has('errors')
                ->etc()
        );
    }

    /** @test */
    public function authenticate_user_can_not_get_product_if_user_has_not_permission()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $product = Product::factory()->create();
        $response = $this->getJson($this->getRouteShow($product->id));
        $response->assertForbidden();
    }

    public function loginUserSuperAdmin()
    {
        $user = User::where('email', 'superadmin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function getRouteShow($id): string
    {
        return route('products.show', $id);
    }
}
