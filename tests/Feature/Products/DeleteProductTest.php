<?php

namespace Tests\Feature\Products;

use Tests\TestCase;
use App\Models\User;
use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteProductTest extends TestCase
{
    public function getRouteDestroy($id)
    {
        return route('products.destroy', $id);
    }
    /** @test */
    public function authenticated_user_can_delete_product_if_product_exist()
    {
        $this->loginUserSuperAdmin();
        $product = Product::factory()->create();
        $countRecord = Product::count();
        $response = $this->delete($this->getRouteDestroy($product->id));
        $response->assertStatus(204);
        $this->assertDatabaseMissing('products', $product->toArray());
        $this->assertDatabaseCount('products', $countRecord - 1);
    }

    /** @test */
    public function authenticated_user_can_not_delete_product_if_product_not_exist()
    {
        $this->loginUserSuperAdmin();
        $response = $this->deleteJson($this->getRouteDestroy(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('errors')
                ->etc()
        );
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_product()
    {
        $product = Product::factory()->create();
        $response = $this->delete($this->getRouteDestroy($product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticate_user_can_not_delete_product_if_admin_has_not_permission()
    {
        $this->loginUserAdmin();
        $product = Product::factory()->create();
        $countRecord = Product::count();
        $response = $this->deleteJson($this->getRouteDestroy($product->id));
        $response->assertForbidden();
        $this->assertDatabaseCount('products', $countRecord);
    }

    /** @test */
    public function authenticate_user_can_not_delete_product_if_user_has_not_permission()
    {
        $this->loginUser();
        $product = Product::factory()->create();
        $countRecord = Product::count();
        $response = $this->deleteJson($this->getRouteDestroy($product->id));
        $response->assertForbidden();
        $this->assertDatabaseCount('products', $countRecord);
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }
    public function loginUserSuperAdmin()
    {
        $user = User::where('email', 'superadmin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUser()
    {
        $user = User::where('email', 'user@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function getRouteDelete($id): string
    {
        return route('products.delete', $id);
    }

    public function makeData()
    {
        return Product::factory()->create();
    }
}
