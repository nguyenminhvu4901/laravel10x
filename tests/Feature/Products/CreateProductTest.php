<?php

namespace Tests\Feature\Products;

use Tests\TestCase;
use App\Models\User;
use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Http\Testing\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;


class CreateProductTest extends TestCase
{
    /** @test */
    public function unauthenticated_user_can_not_view_create_form(): void
    {
        $response = $this->get($this->getRouteCreate());

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }


    /** @test  */
    public function authenticate_user_can_view_create_form(): void
    {
        $this->loginUserSuperAdmin();
        $response = $this->getJson($this->getRouteCreate());
        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(
            fn (AssertableJson $json) => $json->has('status_code')->has('data')
        );

        $response->assertJsonStructure([
            'status_code',
            'data' => [
                '*' => [
                    'id', 'name', 'parent_id'
                ],
            ]
        ]);
    }

    /** @test */
    public function authenticate_user_can_create_new_product(): void
    {
        $this->loginUserSuperAdmin();
        $countRecord = Product::count();
        $dataProduct = $this->makeData();
        $dataProduct['image'] = $this->makeFile();
        $response = $this->postJson($this->getRouteStore(), $dataProduct);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson(
            fn (AssertableJson $json) => $json->where('status_code', Response::HTTP_CREATED)
                ->has('message')
                ->etc()
        );
        $this->assertDatabaseCount('products', $countRecord + 1);
        $this->assertDatabaseHas('products', [
            'name' => $dataProduct['name'],
            'price' => $dataProduct['price'],
            'description' => $dataProduct['description'],
            'expiry' => $dataProduct['expiry'],
            'image' => $dataProduct['image']->name,
        ]);
    }

    /** @test */
    public function authenticated_user_can_not_create_product_if_name_is_null()
    {
        $this->loginUserSuperAdmin();
        $dataProduct = Product::factory()->make([
            'name' => null
        ])->toArray();
        $dataProduct['image'] = $dataProduct['image'] = $this->makeFile();
        $response = $this->postJson($this->getRouteStore(), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('errors')
                ->where('errors.name', fn ($name) => $name->contains('Name không được để trống'))
                ->etc()
        );
    }

    /** @test */
    public function authenticated_user_can_not_create_product_if_price_is_not_invalid()
    {
        $this->loginUserSuperAdmin();
        $dataProduct = Product::factory()->make([
            'price' => 'haha123'
        ])->toArray();
        $dataProduct['image'] = $dataProduct['image'] = $this->makeFile();
        $response = $this->postJson($this->getRouteStore(), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('errors')
                ->where('errors.price', fn ($name) => $name->contains('Price phải là chữ số'))
                ->etc()
        );
    }

    /** @test */
    public function authenticated_user_can_not_create_product_if_price_is_null()
    {
        $this->loginUserSuperAdmin();
        $dataProduct = Product::factory()->make([
            'price' => null
        ])->toArray();
        $dataProduct['image'] = $dataProduct['image'] = $this->makeFile();
        $response = $this->postJson($this->getRouteStore(), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('errors')
                ->where('errors.price', fn ($name) => $name->contains('Price không được để trống'))
                ->etc()
        );
    }

    /** @test */
    public function authenticated_user_can_not_create_product_if_description_is_not_invalid()
    {
        $this->loginUserSuperAdmin();
        $dataProduct = Product::factory()->make([
            'description' => 1233
        ])->toArray();
        $dataProduct['image'] = $this->makeFile();
        $response = $this->postJson($this->getRouteStore(), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('errors')
                ->where('errors.description', fn ($name) => $name->contains('Description không phải là số'))
                ->etc()
        );
    }

    /** @test */
    public function authenticated_user_can_not_create_product_if_description_is_null()
    {
        $this->loginUserSuperAdmin();
        $dataProduct = Product::factory()->make([
            'description' => null
        ])->toArray();
        $dataProduct['image'] = $this->makeFile();
        $response = $this->postJson($this->getRouteStore(), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('errors')
                ->where('errors.description', fn ($name) => $name->contains('Description không được để trống'))
                ->etc()
        );
    }

    /** @test */
    public function authenticated_user_can_not_create_product_if_expiry_is_not_invalid()
    {
        $this->loginUserSuperAdmin();
        $dataProduct = Product::factory()->make([
            'expiry' => 1 - 1 - 01
        ])->toArray();
        $dataProduct['image'] = $this->makeFile();
        $response = $this->postJson($this->getRouteStore(), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('errors')
                ->where('errors.expiry', fn ($name) => $name->contains('Expiry sai định dạng'))
                ->etc()
        );
    }

    /** @test */
    public function authenticated_user_can_not_create_product_if_image_is_max_size()
    {
        $this->loginUserSuperAdmin();
        $dataProduct = Product::factory()->make()->toArray();
        $file = UploadedFile::fake()->create('image.jpg', 10000);
        $dataProduct['image'] = $file;
        $response = $this->postJson($this->getRouteStore(), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('errors')
                ->where('errors.image', fn ($name) => $name->contains('Quá dung lượng ảnh'))
                ->etc()
        );
    }

    /** @test */
    public function authenticated_user_can_not_create_product_if_image_is_not_correct()
    {
        $this->loginUserSuperAdmin();
        $dataProduct = Product::factory()->make()->toArray();
        $file = UploadedFile::fake()->create('image.xls', 4000);
        $dataProduct['image'] = $file;
        $response = $this->postJson($this->getRouteStore(), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('errors')
                ->where('errors.image', fn ($name) => $name->contains('Sai định dạng Image'))
                ->etc()
        );
    }

    /** @test */
    public function authenticate_user_can_not_create_product_if_user_has_not_permission()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $countRecord = Product::count();
        $dataProduct = $this->makeData();
        $dataProduct['image'] = $this->makeFile();
        $response = $this->postJson($this->getRouteStore(), $dataProduct);
        $response->assertForbidden();
        $this->assertDatabaseCount('products', $countRecord);
    }

    public function makeData(): array
    {
        return Product::factory()->make()->toArray();
    }

    public function loginUserSuperAdmin()
    {
        $user = User::where('email', 'superadmin@deha-soft.com')->first();
        $this->actingAs($user);
    }
    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUser()
    {
        $user = User::where('email', 'user@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function makeFile(): File
    {
        Storage::disk('image');
        return UploadedFile::fake()->image(time() . '.jpg');
    }

    public function getRouteCreate()
    {
        return route('products.create');
    }

    public function getRouteStore()
    {
        return route('products.store');
    }

    public function getRouteLogin()
    {
        return route('login');
    }
}
