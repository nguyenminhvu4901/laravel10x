<?php

namespace Tests\Feature\Authenticate;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LogoutTest extends TestCase
{
    /** @test */
    public function authenticate_user_can_logout_page(): void
    {
        $this->loginUserSuperAdmin();

        $response = $this->post($this->getRouteLogout());

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    public function loginUserSuperAdmin()
    {
        $user = User::where('email', 'superadmin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function getRouteLogout()
    {
        return route('logout');
    }

    public function getRouteLogin()
    {
        return route('login');
    }
}
