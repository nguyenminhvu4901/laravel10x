<?php

namespace Tests\Feature\Authenticate;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    /** @test */
    public function unauthenticated_user_can_see_login_form(): void
    {
        $response = $this->get($this->getRouteLogin());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('sessions.create');
    }

    /** @test */
    public function authenticate_user_can_see_dashboard_if_login_successfully(): void
    {
        $response = $this->post($this->getRouteLogin(), [
            'email' => 'user@deha-soft.com',
            'password' => '12345678'
        ]);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteInformation());
    }

    public function getRouteLogin()
    {
        return route('login');
    }

    public function getRouteInformation()
    {
        return route('users.information');
    }
}
