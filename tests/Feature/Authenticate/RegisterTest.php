<?php

namespace Tests\Feature\Authenticate;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegisterTest extends TestCase
{
    /** @test */
    public function unauthenticate_user_can_see_register_form(): void
    {
        $response = $this->get($this->getRouteRegister());

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('register.create');
    }

    /** @test */
    public function unauthenticate_user_can_register_new_user(): void
    {
        $dataRegister = User::factory()->make()->toArray();

        $beforeRegister = User::count();
        $response = $this->post($this->getRouteProcessRegister(), $dataRegister);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('users.information'));
        $this->assertDatabaseCount('users', $beforeRegister + 1);
    }

    /** @test */
    public function unauthenticate_user_can_not_register_new_user_if_field_name_is_null(): void
    {
        $dataRegister = User::factory()->make(['name' => null])->toArray();
        $response = $this->post($this->getRouteProcessRegister(), $dataRegister);

        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function unauthenticate_user_can_not_register_new_user_if_user_email_is_exist(): void
    {
        $dataRegister = User::factory()->make(['email' => 'superadmin@deha-soft.com'])->toArray();
        $response = $this->post($this->getRouteProcessRegister(), $dataRegister);

        $response->assertSessionHasErrors('email');
    }

    /** @test */
    public function unauthenticate_user_can_not_register_new_user_if_field_email_is_null(): void
    {
        $dataRegister = User::factory()->make(['email' => ''])->toArray();
        $response = $this->post($this->getRouteProcessRegister(), $dataRegister);

        $response->assertSessionHasErrors('email');
    }

    /** @test */
    public function unauthenticate_user_can_not_register_new_user_if_field_password_is_null(): void
    {
        $dataRegister = [
            'name' => 'testregister2',
            'email' => 'testthu123@deha-soft.com',
            'phone' => '0961436958',
            'password' => '',
        ];

        $response = $this->post($this->getRouteProcessRegister(), $dataRegister);

        $response->assertSessionHasErrors('password');
    }

    /** @test */
    public function unauthenticate_user_can_not_register_new_user_if_field_phone_is_not_valid(): void
    {
        $dataRegister = User::factory()->make(['phone' => '123456789'])->toArray();
        $response = $this->post($this->getRouteProcessRegister(), $dataRegister);

        $response->assertSessionHasErrors('phone');
    }

    public function getRouteProcessRegister()
    {
        return route('processRegister');
    }

    public function getRouteRegister()
    {
        return route('register');
    }
}
