<?php

namespace Tests\Feature\Users;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateUserTest extends TestCase
{
    public function getRouteCreate()
    {
        return route('users.create');
    }

    public function getRouteStore()
    {
        return route('users.store');
    }

    public function getRouteLogin()
    {
        return route('login');
    }

    /** @test  */
    public function unauthenticated_user_can_not_view_create_user_form(): void
    {
        $response = $this->get($this->getRouteCreate());

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test  */
    public function authenticate_user_can_view_create_user_form_if_super_admin_has_permission(): void
    {
        $this->loginUserSuperAdmin();
        $response = $this->get($this->getRouteCreate());

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admins.users.create');
    }

    /** @test  */
    public function authenticate_user_can_view_create_user_form_if_admin_has_permission(): void
    {
        $this->loginUserAdmin();
        $response = $this->get($this->getRouteCreate());

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test  */
    public function authenticate_user_can_view_create_user_form_if_user_has_permission(): void
    {
        $this->loginUser();
        $response = $this->get($this->getRouteCreate());

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticate_user_can_create_new_user(): void
    {
        $this->loginUserSuperAdmin();
        $dataCreate = User::factory()->make()->toArray();
        $beforeCreate = User::count();
        $response = $this->from($this->getRouteCreate())->post($this->getRouteStore(), $dataCreate);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseCount('users', $beforeCreate + 1);
        $response->assertRedirect(route('users.index'));
    }

    /** @test */
    public function authenticate_user_can_not_create_new_user_if_field_name_null(): void
    {
        $this->loginUserSuperAdmin();
        $dataCreate = user::factory()->make(['name' => null])->toArray();
        $response = $this->from($this->getRouteCreate())->post($this->getRouteStore(), $dataCreate);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteCreate());
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_user_can_not_create_new_user_if_user_email_is_exist(): void
    {
        $this->loginUserSuperAdmin();
        $dataCreate = user::factory()->make(['email' => 'superadmin@deha-soft.com'])->toArray();
        $response = $this->from($this->getRouteCreate())->post($this->getRouteStore(), $dataCreate);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteCreate());
        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function authenticate_user_can_not_create_new_user_if_field_email_is_null(): void
    {
        $this->loginUserSuperAdmin();
        $dataCreate = user::factory()->make(['email' => null])->toArray();
        $response = $this->from($this->getRouteCreate())->post($this->getRouteStore(), $dataCreate);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteCreate());
        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function authenticate_user_can_not_create_new_user_if_field_phone_is_not_Vietnam_phone(): void
    {
        $this->loginUserSuperAdmin();
        $dataCreate = user::factory()->make(['phone' => '12345678901'])->toArray();
        $response = $this->from($this->getRouteCreate())->post($this->getRouteStore(), $dataCreate);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteCreate());
        $response->assertSessionHasErrors(['phone']);
    }

    /** @test */
    public function authenticate_user_can_not_create_new_user_if_field_password_is_null(): void
    {
        $this->loginUserSuperAdmin();
        $dataCreate = [
            'name' => 'Vu',
            'email' => 'vuhaha@deha-soft.com',
            'phone' => '0961435812',
            'password' => '',
        ];
        $response = $this->from($this->getRouteCreate())->post($this->getRouteStore(), $dataCreate);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteCreate());
        $response->assertSessionHasErrors(['password']);
    }

    public function loginUserSuperAdmin()
    {
        $user = User::where('email', 'superadmin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUser()
    {
        $user = User::where('email', 'user@deha-soft.com')->first();
        $this->actingAs($user);
    }
}
