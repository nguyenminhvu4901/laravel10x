<?php

namespace Tests\Feature\Users;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetListUserTest extends TestCase
{

    public function getRouteIndex()
    {
        return route('users.index');
    }

    public function getRouteLogin()
    {
        return route('login');
    }
    /** @test */
    public function unauthenticated_user_can_not_view_all_user(): void
    {
        $response = $this->get($this->getRouteIndex());

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_view_all_user_if_super_admin_has_permission(): void
    {
        $this->loginUserSuperAdmin();
        $response = $this->get($this->getRouteIndex());

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admins.users.index');
    }

    /** @test */
    public function authenticate_user_can_view_all_user_if_admin_has_permission(): void
    {
        $this->loginUserAdmin();
        $response = $this->get($this->getRouteIndex());

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticate_user_can_view_all_user_if_user_has_permission(): void
    {
        $this->loginUser();
        $response = $this->get($this->getRouteIndex());

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function loginUserSuperAdmin()
    {
        $user = User::where('email', 'superadmin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUser()
    {
        $user = User::where('email', 'user@deha-soft.com')->first();
        $this->actingAs($user);
    }
}
