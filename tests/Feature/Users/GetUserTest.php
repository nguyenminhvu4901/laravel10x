<?php

namespace Tests\Feature\Users;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetUserTest extends TestCase
{
    public function getRouteShow($id)
    {
        return route('users.show', $id);
    }

    public function getRouteLogin()
    {
        return route('login');
    }
    /** @test */
    public function unauthenticated_user_can_not_show_user(): void
    {
        $user = User::factory()->create();
        $response = $this->get($this->getRouteShow($user->id));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticated_user_can_show_user_if_super_admin_has_permission(): void
    {
        $this->loginUserSuperAdmin();
        $user = User::factory()->create();
        $response = $this->get($this->getRouteShow($user->id));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admins.users.show');
        $response->assertViewHas('user', $user)
            ->assertSee($user->id)
            ->assertSee($user->name);
    }

    /** @test */
    public function authenticated_user_can_not_show_user_if_admin_has_not_permission(): void
    {
        $this->loginUserAdmin();
        $user = User::factory()->create();
        $response = $this->get($this->getRouteShow($user->id));

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_user_can_not_show_user_if_user_has_not_permission(): void
    {
        $this->loginUser();
        $user = User::factory()->create();
        $response = $this->get($this->getRouteShow($user->id));

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_user_can_not_show_user_if_user_not_exist(): void
    {
        $this->loginUserSuperAdmin();
        $userID = -1;
        $response = $this->get($this->getRouteShow($userID));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function loginUserSuperAdmin()
    {
        $user = User::where('email', 'superadmin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUser()
    {
        $user = User::where('email', 'user@deha-soft.com')->first();
        $this->actingAs($user);
    }
}
