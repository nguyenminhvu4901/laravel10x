<?php

namespace Tests\Feature\Users;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteUserTest extends TestCase
{

    public function getRouteDestroy($id)
    {
        return route('users.destroy', $id);
    }

    public function getRouteLogin()
    {
        return route('login');
    }

    public function getRouteIndex()
    {
        return route('users.index');
    }
    /** @test */
    public function unauthenticated_user_can_not_delete_user(): void
    {
        $user = User::factory()->create();
        $response = $this->delete($this->getRouteDestroy($user->id));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_delete_user_if_super_admin_has_permission(): void
    {
        $this->loginUserSuperAdmin();
        $user = User::factory()->create();
        $beforeDelete = User::count();

        $response = $this->delete($this->getRouteDestroy($user->id));
        $afterDelete = User::count();

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertEquals($beforeDelete - 1, $afterDelete);
        $response->assertRedirect($this->getRouteIndex());
    }

    /** @test */
    public function authenticate_user_can_not_delete_user_if_admin_has_not_permission(): void
    {
        $this->loginUserAdmin();
        $user = User::factory()->create();
        $beforeDelete = User::count();

        $response = $this->delete($this->getRouteDestroy($user->id));
        $afterDelete = User::count();

        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $this->assertEquals($beforeDelete, $afterDelete);
    }

    /** @test */
    public function authenticate_user_can_not_delete_user_if_user_has_not_permission(): void
    {
        $this->loginUser();
        $user = User::factory()->create();
        $beforeDelete = User::count();

        $response = $this->delete($this->getRouteDestroy($user->id));
        $afterDelete = User::count();

        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $this->assertEquals($beforeDelete, $afterDelete);
    }

    /** @test */
    public function authenticate_user_can_not_delete_user_if_user_not_exist(): void
    {
        $this->loginUserSuperAdmin();
        $userID = -1;
        $response = $this->delete($this->getRouteDestroy($userID));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function loginUserSuperAdmin()
    {
        $user = User::where('email', 'superadmin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUser()
    {
        $user = User::where('email', 'user@deha-soft.com')->first();
        $this->actingAs($user);
    }
}
