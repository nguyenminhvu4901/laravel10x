<?php

namespace Tests\Feature\Users;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateUserTest extends TestCase
{
    public function getRouteEdit($id)
    {
        return route('users.edit', $id);
    }

    public function getRouteUpdate($id)
    {
        return route('users.update', $id);
    }

    public function getRouteLogin()
    {
        return route('login');
    }

    public function getRouteIndex()
    {
        return route('users.index');
    }

    /** @test */
    public function unauthenticated_user_can_not_view_update_user_form(): void
    {
        $user = User::factory()->create();
        $response = $this->get($this->getRouteEdit($user->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticated_user_can_view_update_user_form(): void
    {
        $this->loginUserSuperAdmin();
        $user = User::factory()->create();
        $response = $this->get($this->getRouteEdit($user->id));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admins.users.edit');
        $response->assertViewHas('user', $user);
    }

    /** @test */
    public function authenticated_user_can_not_see_view_update_user_form_if_user_is_not_exist(): void
    {
        $this->loginUserSuperAdmin();
        $response = $this->get($this->getRouteEdit(-1));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticated_user_can_not_update_user_form_if_user_is_not_exist(): void
    {
        $this->loginUserSuperAdmin();
        $dataUpdate = User::factory()->make()->toArray();
        $response = $this->from($this->getRouteEdit(-1))->put($this->getRouteUpdate(-1), $dataUpdate);

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticate_user_can_update_user_if_super_admin_has_permission(): void
    {
        $this->loginUserSuperAdmin();
        $user = User::factory()->create();
        $dataUpdate = User::factory()->make()->toArray();
        $response = $this->from($this->getRouteEdit($user->id))->put($this->getRouteUpdate($user->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteIndex());
    }

    /** @test */
    public function authenticate_user_can_not_update_user_if_admin_has_not_permission(): void
    {
        $this->loginUserAdmin();
        $user = User::factory()->create();
        $dataUpdate = User::factory()->make()->toArray();
        $response = $this->from($this->getRouteEdit($user->id))->put($this->getRouteUpdate($user->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticate_user_can_not_update_user_if_user_has_not_permission(): void
    {
        $this->loginUser();
        $user = User::factory()->create();
        $dataUpdate = User::factory()->make()->toArray();
        $response = $this->from($this->getRouteEdit($user->id))->put($this->getRouteUpdate($user->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticate_user_can_not_update_user_if_field_name_null(): void
    {
        $this->loginUserSuperAdmin();
        $user = User::factory()->create();
        $dataUpdate = User::factory()->make(['name' => null])->toArray();
        $response = $this->from($this->getRouteEdit($user->id))->put($this->getRouteUpdate($user->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteEdit($user->id));
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_user_can_not_update_user_if_field_phone_number_null(): void
    {
        $this->loginUserSuperAdmin();
        $user = User::factory()->create();
        $dataUpdate = User::factory()->make(['phone' => null])->toArray();
        $response = $this->from($this->getRouteEdit($user->id))->put($this->getRouteUpdate($user->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteEdit($user->id));
        $response->assertSessionHasErrors(['phone']);
    }

    /** @test */
    public function authenticate_user_can_not_update_user_if_field_email_number_null(): void
    {
        $this->loginUserSuperAdmin();
        $user = User::factory()->create();
        $dataUpdate = User::factory()->make(['email' => null])->toArray();
        $response = $this->from($this->getRouteEdit($user->id))->put($this->getRouteUpdate($user->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteEdit($user->id));
        $response->assertSessionHasErrors(['email']);
    }

    public function loginUserSuperAdmin()
    {
        $user = User::where('email', 'superadmin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUser()
    {
        $user = User::where('email', 'user@deha-soft.com')->first();
        $this->actingAs($user);
    }
}
