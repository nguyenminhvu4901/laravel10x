<?php

namespace Tests\Feature\Roles;

use Tests\TestCase;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetRoleTest extends TestCase
{
    /** @test */
    public function unauthenticated_user_can_not_show_role(): void
    {
        $role = Role::factory()->create();
        $response = $this->get($this->getRouteShow($role->id));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticated_user_can_show_role_if_super_admin_has_permission(): void
    {
        $this->loginUserSuperAdmin();
        $role = Role::factory()->create();
        $response = $this->get($this->getRouteShow($role->id));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewHas('role', $role)
            ->assertSee($role->name);
    }

    /** @test */
    public function authenticated_user_can_not_show_role_if_role_is_not_exist(): void
    {
        $this->loginUserSuperAdmin();
        $response = $this->get($this->getRouteShow(-1));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticated_user_can_show_role_if_admin_has_not_permission(): void
    {
        $this->loginUserAdmin();
        $role = User::factory()->create();
        $response = $this->get($this->getRouteShow($role->id));

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_user_can_show_role_if_user_has_not_permission(): void
    {
        $this->loginUser();
        $role = User::factory()->create();
        $response = $this->get($this->getRouteShow($role->id));

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }


    public function loginUserSuperAdmin()
    {
        $user = User::where('email', 'superadmin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUser()
    {
        $user = User::where('email', 'user@deha-soft.com')->first();
        $this->actingAs($user);
    }
    public function getRouteShow($id)
    {
        return route('roles.show', $id);
    }

    public function getRouteLogin()
    {
        return route('login');
    }
}
