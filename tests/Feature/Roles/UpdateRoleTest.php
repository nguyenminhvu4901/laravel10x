<?php

namespace Tests\Feature\Roles;

use Tests\TestCase;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateRoleTest extends TestCase
{
    /** @test */
    public function unauthenticate_user_can_not_update_role_form(): void
    {
        $role = Role::factory()->create();
        $response = $this->get($this->getRouteEdit($role->id));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_view_update_role_form(): void
    {
        $this->loginUserSuperAdmin();
        $role = Role::factory()->create();
        $response = $this->get($this->getRouteEdit($role->id));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admins.roles.edit');
        $response->assertViewHas('role', $role)
            ->assertSee($role->name);
    }

    /** @test */
    public function authenticate_user_can_update_role_if_super_admin_has_permission(): void
    {
        $this->loginUserSuperAdmin();
        $role = Role::factory()->create();
        $dataUpdate = Role::factory()->make()->toArray();
        $response = $this->from($this->getRouteEdit($role->id))->put($this->getRouteUpdate($role->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $dataUpdate);
        $response->assertRedirect(route('roles.index'));
    }

    /** @test */
    public function authenticate_user_can_not_update_role_if_admin_has_not_permission(): void
    {
        $this->loginUserAdmin();
        $role = Role::factory()->create();
        $dataUpdate = Role::factory()->make()->toArray();
        $response = $this->from($this->getRouteEdit($role->id))->put($this->getRouteUpdate($role->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticate_user_can_not_update_role_if_user_has_not_permission(): void
    {
        $this->loginUser();
        $role = Role::factory()->create();
        $dataUpdate = Role::factory()->make()->toArray();
        $response = $this->from($this->getRouteEdit($role->id))->put($this->getRouteUpdate($role->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticate_user_can_not_update_role_if_field_name_null(): void
    {
        $this->loginUserSuperAdmin();
        $role = Role::factory()->create();
        $dataUpdate = Role::factory()->make(['name' => null])->toArray();
        $response = $this->from($this->getRouteEdit($role->id))->put($this->getRouteUpdate($role->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteEdit($role->id));
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_user_can_not_update_role_if_role_is_not_exist(): void
    {
        $this->loginUserSuperAdmin();
        $role = -1;
        $response = $this->get($this->getRouteEdit($role));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function loginUserSuperAdmin()
    {
        $user = User::where('email', 'superadmin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUser()
    {
        $user = User::where('email', 'user@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function getRouteEdit($id)
    {
        return route('roles.edit', $id);
    }

    public function getRouteUpdate($id)
    {
        return route('roles.update', $id);
    }

    public function getRouteLogin()
    {
        return route('login');
    }
}
