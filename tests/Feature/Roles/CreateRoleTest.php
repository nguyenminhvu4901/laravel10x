<?php

namespace Tests\Feature\Roles;

use Tests\TestCase;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateRoleTest extends TestCase
{
    /** @test */
    public function unauthenticate_user_can_not_create_role_form(): void
    {
        $Response = $this->post($this->getRouteStore());
        $Response->assertStatus(Response::HTTP_FOUND);
        $Response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_create_role_form_if_super_admin_has_permission(): void
    {
        $this->loginUserSuperAdmin();
        $dataCreate = Role::factory()->make()->toArray();
        $dataBeforeCreate = Role::count();
        $response = $this->from($this->getRouteCreate())->post($this->getRouteStore(), $dataCreate);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $dataCreate);
        $response->assertRedirect(route('roles.index'));
        $this->assertDatabaseCount('roles', $dataBeforeCreate + 1);
    }

    /** @test */
    public function authenticate_user_can_not_create_role_form_if_field_name_null(): void
    {
        $this->loginUserSuperAdmin();
        $dataCreate = Role::factory()->make(['name' => null])->toArray();
        $response = $this->from($this->getRouteCreate())->post($this->getRouteStore(), $dataCreate);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('roles.create'));
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_user_can_not_create_role_form_if_field_display_name_null(): void
    {
        $this->loginUserSuperAdmin();
        $dataCreate = Role::factory()->make(['display_name' => null])->toArray();
        $response = $this->from($this->getRouteCreate())->post($this->getRouteStore(), $dataCreate);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('roles.create'));
        $response->assertSessionHasErrors(['display_name']);
    }

    /** @test */
    public function authenticate_user_can_not_create_role_form_if_admin_has_not_permission(): void
    {
        $this->loginUserAdmin();
        $dataCreate = Role::factory()->make()->toArray();
        $response = $this->from($this->getRouteCreate())->post($this->getRouteStore(), $dataCreate);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticate_user_can_not_create_role_form_if_user_has_not_permission(): void
    {
        $this->loginUser();
        $dataCreate = Role::factory()->make()->toArray();
        $response = $this->from($this->getRouteCreate())->post($this->getRouteStore(), $dataCreate);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }


    public function loginUserSuperAdmin()
    {
        $user = User::where('email', 'superadmin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUser()
    {
        $user = User::where('email', 'user@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function getRouteCreate()
    {
        return route('roles.create');
    }

    public function getRouteStore()
    {
        return route('roles.store');
    }

    public function getRouteLogin()
    {
        return route('login');
    }
}
