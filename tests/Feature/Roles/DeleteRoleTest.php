<?php

namespace Tests\Feature\Roles;

use Tests\TestCase;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteRoleTest extends TestCase
{
    public function getRouteDestroy($id)
    {
        return route('roles.destroy', $id);
    }

    public function getRouteLogin()
    {
        return route('login');
    }

    public function getRouteIndex()
    {
        return route('roles.index');
    }
    /** @test */
    public function unauthenticate_user_can_not_delete_role(): void
    {
        $role = Role::factory()->create();
        $response = $this->delete($this->getRouteDestroy($role->id));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_delete_role_if_super_admin_has_permission(): void
    {
        $this->loginUserSuperAdmin();
        $role = Role::factory()->create();
        $beforeDelete = Role::count();

        $response = $this->delete($this->getRouteDestroy($role->id));
        $afterDelete = Role::count();

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertEquals($beforeDelete - 1, $afterDelete);
        $response->assertRedirect($this->getRouteIndex());
    }

    /** @test */
    public function authenticate_user_can_not_delete_role_if_admin_has_not_permission(): void
    {
        $this->loginUserAdmin();
        $role = Role::factory()->create();

        $response = $this->delete($this->getRouteDestroy($role->id));

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticate_user_can_not_delete_role_if_role_is_not_exist(): void
    {
        $this->loginUserSuperAdmin();
        $role = -1;
        $response = $this->delete($this->getRouteDestroy(-1));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticate_user_can_not_delete_role_if_user_has_not_permission(): void
    {
        $this->loginUser();
        $role = Role::factory()->create();
        $response = $this->delete($this->getRouteDestroy($role->id));

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function loginUserSuperAdmin()
    {
        $user = User::where('email', 'superadmin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUser()
    {
        $user = User::where('email', 'user@deha-soft.com')->first();
        $this->actingAs($user);
    }
}
