<?php

namespace Tests\Feature\Categories;

use Tests\TestCase;
use App\Models\User;
use App\Models\Category;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteCategoyTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_delete_category_if_category_exists()
    {
        $this->loginUserSuperAdmin();
        $category = $this->createCategory();
        $countData = Category::count();
        $response = $this->delete($this->getRouteDestroy($category->id));

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseMissing('categories', $category->toArray());
        $this->assertDatabaseCount('categories', $countData - 1);
        $response->assertRedirect($this->getRouteIndex());
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_category()
    {
        $category = $this->createCategory();
        $response = $this->delete($this->getRouteDestroy($category->id));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticated_user_can_not_delete_category_if_category_not_exists()
    {
        $this->loginUserSuperAdmin();
        $id = -1;
        $response = $this->delete($this->getRouteDestroy($id));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticate_user_can_not_delete_category_if_user_has_not_permission()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $category = $this->createCategory();
        $countData = Category::count();
        $response = $this->delete($this->getRouteDestroy($category->id));

        $response->assertForbidden();
        $this->assertDatabaseCount('categories', $countData);
    }

    public function createCategory()
    {
        return Category::factory()->create();
    }
    public function makeData(): array
    {
        return Category::factory()->make()->toArray();
    }

    public function loginUserSuperAdmin()
    {
        $user = User::where('email', 'superadmin@deha-soft.com')->first();
        $this->actingAs($user);
    }
    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUser()
    {
        $user = User::where('email', 'user@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function getRouteDestroy($id)
    {
        return route('categories.destroy', $id);
    }

    public function getRouteIndex()
    {
        route('categories.index');
    }

    public function getRouteLogin()
    {
        return route('login');
    }
}
