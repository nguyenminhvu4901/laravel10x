<?php

namespace Tests\Feature\Categories;

use Tests\TestCase;
use App\Models\User;
use App\Models\Category;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateCategoyTest extends TestCase
{
    public function getRouteIndex()
    {
        return route('categories.index');
    }

    /** @test */
    public function authenticated_user_can_create_category_if_super_admin_has_permission()
    {
        $this->loginUserSuperAdmin();
        $data = $this->makeData();
        $countData = Category::count();
        $response = $this->from($this->getRouteCreate())->post($this->getRouteStore(), $data);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('categories', $data);
        $response->assertRedirect($this->getRouteIndex());
        $this->assertDatabaseCount('categories', $countData + 1);
    }

    /** @test */
    public function authenticated_user_can_create_category_if_admin_has_permission()
    {
        $this->loginUserAdmin();
        $data = $this->makeData();
        $countData = Category::count();
        $response = $this->from($this->getRouteCreate())->post($this->getRouteStore(), $data);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('categories', $data);
        $response->assertRedirect($this->getRouteIndex());
        $this->assertDatabaseCount('categories', $countData + 1);
    }

    /** @test */
    public function authenticated_user_can_not_create_category_if_user_has_permission()
    {
        $this->loginUser();
        $data = $this->makeData();
        $response = $this->from($this->getRouteCreate())->post($this->getRouteStore(), $data);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_user_can_create_category()
    {
        $this->loginUserSuperAdmin();
        $data = $this->makeData();
        $countData = Category::count();
        $response = $this->from($this->getRouteCreate())->post($this->getRouteStore(), $data);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('categories', $data);
        $response->assertRedirect($this->getRouteIndex());
        $this->assertDatabaseCount('categories', $countData + 1);
    }

    /** @test */
    public function unauthenticate_user_can_not_create_new_category()
    {
        $data = $this->makeData();
        $response = $this->from($this->getRouteCreate())->post($this->getRouteStore(), $data);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function unauthenticate_user_can_not_see_create_new_category_form()
    {
        $data = $this->makeData();
        $response = $this->get($this->getRouteCreate());

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_not_create_new_category_if_data_is_not_valid()
    {
        $this->loginUserSuperAdmin();

        $data = Category::factory()->make(['name' => ''])->toArray();
        $response = $this->from($this->getRouteCreate())->post($this->getRouteStore(), $data);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteCreate());
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_user_can_see_create_new_category_form_if_super_admin_has_permission()
    {
        $this->loginUserSuperAdmin();
        $response = $this->get($this->getRouteCreate());

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('clients.categories.create');
    }

    /** @test */
    public function authenticate_user_can_see_create_new_category_form_if_admin_has_permission()
    {
        $this->loginUserAdmin();
        $response = $this->get($this->getRouteCreate());

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('clients.categories.create');
    }

    /** @test */
    public function authenticate_user_can_not_see_create_new_category_form_if_user_has_not_permission()
    {
        $this->loginUser();
        $response = $this->get($this->getRouteCreate());

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function makeData(): array
    {
        return Category::factory()->make()->toArray();
    }

    public function loginUserSuperAdmin()
    {
        $user = User::where('email', 'superadmin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function loginUser()
    {
        $user = User::where('email', 'user@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function getRouteCreate()
    {
        return route('categories.create');
    }

    public function getRouteStore()
    {
        return route('categories.store');
    }

    public function getRouteLogin()
    {
        return route('login');
    }
}
