<?php

namespace Tests\Feature\Categories;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetListCategoyTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_get_list_category()
    {
        $this->loginUserSuperAdmin();
        $response = $this->get($this->getRouteIndex());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('clients.categories.index')
            ->assertSee('id')
            ->assertSee('name')
            ->assertSee('parent_id');
    }

    /** @test */
    public function unauthenticated_user_can_not_get_list_category()
    {
        $response = $this->get($this->getRouteIndex());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_not_get_list_category_if_user_has_not_permission()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $response = $this->get($this->getRouteIndex());
        $response->assertForbidden();
    }

    public function loginUserSuperAdmin()
    {
        $user = User::where('email', 'superadmin@deha-soft.com')->first();
        $this->actingAs($user);
    }
    public function getRouteIndex()
    {
        return route('categories.index');
    }

    public function getRouteLogin()
    {
        return route('login');
    }
}
