<?php

namespace Tests\Feature\Categories;

use Tests\TestCase;
use App\Models\User;
use App\Models\Category;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateCategoyTest extends TestCase
{
    public function getRouteIndex()
    {
        return route('categories.index');
    }

    public function getRouteEdit($id)
    {
        return route('categories.edit', $id);
    }

    public function getRouteUpdate($id)
    {
        return route('categories.update', $id);
    }

    public function getRouteLogin()
    {
        return route('login');
    }
    /** @test */
    public function authenticate_user_can_update_category_if_data_is_valid()
    {
        $this->loginUserSuperAdmin();
        $category = $this->createCategory();
        $data = $this->makeData();
        $response = $this->from($this->getRouteEdit($category->id))->put($this->getRouteUpdate($category->id), $data);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('categories', $data);
        $response->assertRedirect($this->getRouteIndex());
    }

    /** @test */
    public function unauthenticate_user_can_not_update_category()
    {
        $category = $this->createCategory();
        $data = $this->makeData();
        $response = $this->from($this->getRouteEdit($category->id))->put($this->getRouteUpdate($category->id), $data);

        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function unauthenticate_user_can_not_see_update_category_form()
    {
        $category = $this->createCategory();
        $response = $this->get($this->getRouteEdit($category->id));

        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_see_update_category_form()
    {
        $this->loginUserSuperAdmin();
        $category = $this->createCategory();
        $response = $this->get($this->getRouteEdit($category->id));

        $response->assertViewHas('category', $category);
        $response->assertSee($category->name, $category->parent_id);
    }

    /** @test */
    public function authenticate_user_can_not_update_category_if_data_is_not_valid()
    {
        $this->loginUserSuperAdmin();
        $category = $this->createCategory();
        $data = Category::factory()->make(['name' => ''])->toArray();
        $response = $this->from($this->getRouteEdit($category->id))->put($this->getRouteUpdate($category->id), $data);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteEdit($category->id));
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_user_can_not_update_developer_if_category_is_not_exist()
    {
        $this->loginUserSuperAdmin();
        $id = -1;
        $response = $this->get($this->getRouteEdit($id));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function createCategory()
    {
        return Category::factory()->create();
    }

    public function makeData(): array
    {
        return Category::factory()->make()->toArray();
    }

    public function loginUserSuperAdmin()
    {
        $user = User::where('email', 'superadmin@deha-soft.com')->first();
        $this->actingAs($user);
    }
}
